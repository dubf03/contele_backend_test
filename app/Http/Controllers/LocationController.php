<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Support\Facades\Input;

class LocationController extends Controller
{
    protected $model;

    public function __construct(Location $model) {
      $this->model = $model;
    }

    public function locations() {
      $data = Input::all();
      $data['radius'] -= $data['radius'] * ($data['zoom']*10/100);

      if($data['zoom'] <= $data['min']) {
        $data['zoom'] = $data['min'];
      }

      if($data['zoom'] >= $data['max']) {
        $data['zoom'] = $data['max'];
      }

      $data['lng'] = (int) $data['lng'];
      $data['lat'] = (int) $data['lat'];
      $data['view'] = (int) $data['view'];

      return $this->model->locations($data);
    }
}
