<?php

namespace App;

use Illuminate\Support\Facades\Cache;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Location extends Eloquent {

    public function cacheKey($key)
    {
        return sprintf("%s",$key);
    }

    public function locations($data)
    {
        $key = $data['lng']."-".$data['lat']."-".$data['view']."-".$data['radius']."-".$data['zoom'];
        return Cache::remember($this->cacheKey($key) . ':locationsTable', 60, function () use ($data){
			return $this->where('location', 'near', [
			'$geometry' => [
			      'type' => 'Point',
			    'coordinates' => [
			        $data['lng'],
			        $data['lat']
			      ],
			  ],
			  '$maxDistance' => $data['radius'],
			])->take($data['view'])->get();
        });
    }


}